package org.binar.firebase.controller;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FirebaseMessageController {

    @Autowired
    FirebaseMessaging firebaseMessaging;

    @PostMapping("/send-message")
    public ResponseEntity sendMessage() throws FirebaseMessagingException {
        Message msg = Message.builder()
                .putData("body", "ini notif")
                .setTopic("demo.firebase")
                .build();
        String id = FirebaseMessaging.getInstance().send(msg);
        return new ResponseEntity(id, HttpStatus.OK);
    }
}
